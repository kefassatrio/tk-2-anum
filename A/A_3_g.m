function result = A_3_g(x)
    for i=0:50
      y = x**2 + x - cos(pi*x)
      x = y
    endfor
    result = x;
end
