function result = A_3_f(x)
    for i=0:50
      y = cos(pi*x) - x**2 - x
      x = y
    endfor
    result = x;
end
