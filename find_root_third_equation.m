function find_root_third_equation(x, tol)
  init_value = x;
  iteration = 1;
  while abs(third_function(init_value)) > tol
    xn = normal_newton(init_value);
    init_value = xn;
    iteration++;
  endwhile
  iteration
  init_value
  third_function(init_value)