function result = newton_g(w,x,y,z)
    m = [w;x;y;z];
    for i=0:4
      w = m(1,:);
      x = m(2,:);
      y = m(3,:);
      z = m(4,:);
      
      
      
      g_ww = -2;
      g_wx = 1;
      g_wy = 0;
      g_wz = 0;
      
      g_xw = 1;
      g_xx = -2;
      g_xy = 1;
      g_xz = 0;
      
      g_yw = 0;
      g_yx = 1;
      g_yy = -2;
      g_yz = 1;
      
      g_zw = 0;
      g_zx = 0;
      g_zy = 1;
      g_zz = -2;
      


      hessian = [g_ww g_xw g_yw g_zw;g_wx g_xx g_yx g_zx; g_wy g_xy g_yy g_zy;g_wz g_xz g_yz g_zz];
      delta = [f_C_g_w(w,x,y,z);f_C_g_x(w,x,y,z);f_C_g_y(w,x,y,z);f_C_g_z(w,x,y,z)];
      hessian = inverse(hessian);
      right_hand = hessian*delta;
      left_hand = [w;x;y;z];
      m = left_hand - right_hand;
      w = m(1,:);
      x = m(2,:);
      y = m(3,:);
      z = m(4,:);
    endfor
      w = m(1,:);
      x = m(2,:);
      y = m(3,:);
      z = m(4,:);
    result = [w;x;y;z];
end
