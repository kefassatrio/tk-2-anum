function result = newton_A_1(x)
    prev_x = -1;
    new_x = 0;
    while (prev_x != new_x)
      prev_x = x;
      new_x = x - (f_A_1(x)/f_A_1_d(x));
      x = new_x;
    endwhile
    result = x;
end
