function result = newton_f(x,y)
    m = [x;y];
    for i=0:49
      x = m(1,:);
      y = m(2,:);
      f_xx = (y**2)*(e**(x*y)) + 2;
      f_xy = ((x*y+1)*e**(x*y))-1;
      f_yx = ((x*y+1)*e**(x*y))-1;
      f_yy = (x**2)*(e**(x*y)) + 2;
      hessian = [f_xx f_xy;f_yx f_yy];
      delta = [f_C_f_x(x,y);f_C_f_y(x,y)];
      hessian = inverse(hessian);
      right_hand = hessian*delta;
      left_hand = [x;y];
      m = left_hand - right_hand;
    endfor
    x = m(1,:);
    y = m(2,:);
    result = [x;y];
end
