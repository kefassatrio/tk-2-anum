function result = f_B_3(u,v,w)
    result = 3*u^2 - 12*u + u*v^2 + 3*w^2 + 2;
end
