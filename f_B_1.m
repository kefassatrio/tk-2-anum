function result = f_B_1(u,v,w)
    result = 2*u^2 - 4*u + v^2 + 3*v*w^2 + 6*w - 2;
end
