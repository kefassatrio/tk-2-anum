function result = dfdw_B2_3(u,u2,v,v2,w,w2)
    u3 = (1/2)*(u+u2);
    v3 = (1/2)*(v+v2);
    w3 = (1/2)*(w+w2);
    result = 6*w3;
end
