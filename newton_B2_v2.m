function result = newton_B2_v2(u0,v0,w0)
    x = [u0 v0 w0]';
    x2 = [u0 v0 w0]';
    f = f_B1(u0,v0,w0);
    x = x - inv(jacobian_B2(x(1,1),x2(1,1),x(2,1),x2(2,1),x(3,1),x2(3,1)))*f;
    f = f_B1(x(1,1),x(2,1),x(3,1));
    for i = 1:10
      i
      x2 = x - inv(jacobian_B2(x(1,1),x2(1,1),x(2,1),x2(2,1),x(3,1),x2(3,1)))*f;
      x = x - inv(jacobian_B2(x(1,1),x2(1,1),x(2,1),x2(2,1),x(3,1),x2(3,1)))*f
      f = f_B1(x(1,1),x(2,1),x(3,1));
    endfor
    result = x;
end
