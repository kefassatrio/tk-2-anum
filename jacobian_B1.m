function result = jacobian_B1(u,v,w)
    result = [ dfdu_B_1(u,v,w) dfdv_B_1(u,v,w) dfdw_B_1(u,v,w) ;
                 dfdu_B_2(u,v,w) dfdv_B_2(u,v,w) dfdw_B_2(u,v,w) ;
                 dfdu_B_3(u,v,w) dfdv_B_3(u,v,w) dfdw_B_3(u,v,w) ];
end
