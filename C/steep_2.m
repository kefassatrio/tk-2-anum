function result = steep_2(w,x,y,z,h)
    g = (w+1)*(x+1)+(x+1)*(y+1)+(y+1)*(z+1)-w**2-x**2-y**2-z**2;
    for i=0:49
      i = f_C_g_w(w,x,y,z);
      j = f_C_g_x(w,x,y,z);
      k = f_C_g_y(w,x,y,z);
      l = f_C_g_z(w,x,y,z);
      
      w = w + (h*i);
      x = x + (h*j);
      y = y + (h*k);
      z = z + (h*l);
    endfor
    g = (w+1)*(x+1)+(x+1)*(y+1)+(y+1)*(z+1)-w**2-x**2-y**2-z**2;
    result = [w,x,y,z];
end
