function result = qr_A6()
  A = [ 0 1 0 0 0 0 0 ;
        0 0 1 0 0 0 0 ;
        0 0 0 1 0 0 0 ;
        0 0 0 0 1 0 0 ;
        0 0 0 0 0 1 0 ;
        0 0 0 0 0 0 1 ;
        27 0 -72 19 35 -2 -6 ]
  for i = 1:100
      [Q,R]=qr(A);
      A=R*Q;
  endfor
  A
  result = diag(A);
end
