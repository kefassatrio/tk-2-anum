function result = modified_newton(x)
  f = (x**7) + (6*(x**6)) + (2*(x**5)) + (-35*(x**4)) + (-19*(x**3)) + (72*(x**2)) - 27;
  f_der = (7*(x**6)) + (36*(x**5)) + (10*(x**4)) + (-140*(x**3)) + (-57*(x**2)) + (144*x);
  f_sec_der = (42*(x**5)) + (180*(x**4)) + (40*(x**3)) + (-420*(x**2)) + (-114*x) + 144;
  bottom = (f_der**2) - (f * f_sec_der);
  top = f * f_der;
  result = x - (top / bottom);
 end