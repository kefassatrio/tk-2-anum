function result = f_A_2_d(x)
    result = (x**x * (log(x) + 1)) - 1;
end
