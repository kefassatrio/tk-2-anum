function result = third_function(x)
  result = (x**7) + (6*(x**6)) + (2*(x**5)) + (-35*(x**4)) + (-19*(x**3)) + (72*(x**2)) - 27;    