function result = newton_B1(u0,v0,w0)
    x = [u0 v0 w0]'; 
    J = jacobian_B1(u0,v0,w0);
    f = f_B1(u0,v0,w0);
    for i = 1:10
      i
      x = x - inv(J)*f
      f = f_B1(x(1,1),x(2,1),x(3,1));
      J = jacobian_B1(x(1,1),x(2,1),x(3,1));
    endfor
    result = x;
end
